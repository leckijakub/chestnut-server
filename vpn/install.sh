#!/bin/bash


SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
VPN_DIR="${SCRIPT_DIR}"
VPN_DOCKER_DIR="${VPN_DIR}/docker-openvpn"
SYSTEMD_UNIT_DIR="/lib/systemd"
SYSTEMD_SERVICE_TEMPLATE="docker-openvpn.service"
DEST_SERVICE="docker-openvpn.service"
CLIENTNAME="chestnut-client"

read -p "server address/ip:" IP
cd $VPN_DOCKER_DIR && docker-compose run --rm openvpn ovpn_genconfig -u udp://${IP}
cd $VPN_DOCKER_DIR && docker-compose run --rm openvpn ovpn_initpki

cd $VPN_DOCKER_DIR && ./vpn-compose.sh start

cd $VPN_DOCKER_DIR && docker-compose run --rm openvpn easyrsa build-client-full $CLIENTNAME
cd $VPN_DOCKER_DIR && docker-compose run --rm openvpn ovpn_getclient $CLIENTNAME > $CLIENTNAME.ovpn

cp ${SCRIPT_DIR}/${SYSTEMD_SERVICE_TEMPLATE} ${SYSTEMD_UNIT_DIR}/system/${DEST_SERVICE}
sed -e "s|@@VPN_DIR@@|$VPN_DIR|" -i ${SYSTEMD_UNIT_DIR}/system/${DEST_SERVICE}
sudo systemctl enable ${DEST_SERVICE}
