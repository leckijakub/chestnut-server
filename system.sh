#!/bin/bash

######## base config ########

DOTFILES_DIR="~/dotfiles"

# setup dot config
if [ -d "${DOTFILES_DIR}" ]; then
    git clone https://gitlab.com/leckijakub/dotfiles.git
    ${DOTFILES_DIR}/install
fi

# shutdown system on power-button press
hostnamectl set-chassis vm


######## printer setup ########

mkdir printer
cd printer

# install printer management software
sudo apt install cups
sudo apt install lpr

# install printer driver
wget https://download.brother.com/welcome/dlf006893/linux-brprinter-installer-2.2.2-2.gz
gzip -d linux-brprinter-installer-2.2.2-2.gz
chmod +x linux-brprinter-installer-2.2.2-2
sudo ./linux-brprinter-installer-2.2.2-2 HLL2312D

# set default printer
lpoptions -d HLL2310D
# share printer
sudo lpadmin -p HLL2310D -o printer-is-shared=true

# clean up
rm linux-brprinter-installer-2.2.2-2
rm hll2310dpdrv-4.0.0-1.i386.deb
rm hll2310dpdrv-4.0.0-1a.i386.deb

cd ..
