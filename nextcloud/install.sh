#!/bin/bash


SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
NC_DIR="${SCRIPT_DIR}"
NC_DOCKER_DIR="${NC_DIR}/docker-nextcloud"
SYSTEMD_UNIT_DIR="/lib/systemd"
SYSTEMD_SERVICE_TEMPLATE="docker-nextcloud.service"
DEST_SERVICE="docker-nextcloud.service"

cd $NC_DOCKER_DIR
docker-compose build --pull && docker-compose up

cp ${SCRIPT_DIR}/${SYSTEMD_SERVICE_TEMPLATE} ${SYSTEMD_UNIT_DIR}/system/${DEST_SERVICE}
sed -e "s|@@NC_DIR@@|$NC_DIR|" -i ${SYSTEMD_UNIT_DIR}/system/${DEST_SERVICE}
sudo systemctl enable ${DEST_SERVICE}
