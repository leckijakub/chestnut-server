#!/bin/bash

CMD="$1"

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

case $CMD in
    "start")
        echo "Starting ..."
        cd $SCRIPT_DIR && docker-compose pull && docker-compose up
        ;;
    "stop")
        echo "Stopping ..."
        cd $SCRIPT_DIR &&  docker-compose down
        ;;
    *)
echo "Unknown CMD: $CMD"
;;
esac
